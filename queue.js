let collection = [];

// Write the queue functions below.
const enqueue = (name) => {
  collection[collection.length] = name;
  return collection;
};

const dequeue = () => {
  collection.shift();
  return collection;
};

const front = () => {
  return collection[0];
};

const print = () => {
  return collection;
};

const size = () => {
  return collection.length;
};

const isEmpty = () => {
  return collection.length === 0 ? true : false;
};

module.exports = {
  collection,
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty,
};
